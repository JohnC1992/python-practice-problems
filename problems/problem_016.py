# Complete the is_inside_bounds function which takes an x
# coordinate and a y coordinate, and then tests each to
# make sure they're between 0 and 10, inclusive.

def is_inside_bounds(x, y):
    if x < 0 or x > 10:
        return False
    if y < 0 or y > 10:
        return False
    return True

for x in range(-1,12):
    for y in range(-1,12):
        print(f"({x},{y}) inside bounds:{is_inside_bounds(x,y)}")
