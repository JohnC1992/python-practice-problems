# Write a function that meets these requirements.
#
# Name:       shift_letters
# Parameters: a string containing a single word
# Returns:    a new string with all letters replaced
#             by the next letter in the alphabet
#
# If the letter "Z" or "z" appear in the string, then
# they would get replaced by "A" or "a", respectively.
#
# Examples:
#     * inputs:  "import"
#       result:  "jnqpsu"
#     * inputs:  "ABBA"
#       result:  "BCCB"
#     * inputs:  "Kala"
#       result:  "Lbmb"
#     * inputs:  "zap"
#       result:  "abq"
#
# You may want to look at the built-in Python functions
# "ord" and "chr" for this problem

def shift_letters(string):
    new_string = ""
    for char in string:
        if char == "z" or char == "Z":
            new_string += chr(ord(char)-25)
            continue
        new_string += chr(ord(char)+1)
    return new_string

word = "abcdefghijklmnopqrstuvwxyzZAGGFD"
print(shift_letters(word))
