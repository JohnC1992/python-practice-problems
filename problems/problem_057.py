# Write a function that meets these requirements.
#
# Name:       sum_fraction_sequence
# Parameters: a number
# Returns:    the sum of the fractions of the
#             form 1/2+2/3+3/4+...+number/number+1
#
# Examples:
#     * input:   1
#       returns: 1/2
#     * input:   2
#       returns: 1/2 + 2/3
#     * input:   3
#       returns: 1/2 + 2/3 + 3/4

def sum_fraction_sequence(number):
    fractions =""
    for count in range(1, number+1):
        fractions += f"{'+' if count > 1 else ''}{count}/{count+1}"
    return fractions

for i in range(10):
    print(sum_fraction_sequence(i))
