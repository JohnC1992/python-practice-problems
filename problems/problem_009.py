# Complete the is_palindrome function to check if the value in
# the word parameter is the same backward and forward.
#
# For example, the word "racecar" is a palindrome because, if
# you write it backwards, it's the same word.

# It uses the built-in function reversed and the join method
# for string objects.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.
import os

def is_palindrome(word):
    for index in range(len(word)):
        if word[index] != word[len(word)-(index + 1)]:
            return False
    return True

while True:
    os.system('cls')
    print("Type 'exit()' to quit")
    getInput = input("Enter a word:")
    if getInput == "exit()":
        break
    if is_palindrome(getInput):
        print(f"{getInput} is a palindrome")
    else:
        print(f"{getInput} is not a palindrome")
    waitForConfirmation = input("Press enter to continue...")
