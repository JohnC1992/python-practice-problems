# Complete the find_second_largest function which accepts
# a list of numerical values and returns the second largest
# in the list
#
# If the list of values is empty, the function should
# return None
#
# If the list of values has only one value, the function
# should return None
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def find_second_largest(values):
    if len(values) < 2:
        return None
    largest = max(values)
    second_largest = min(values)
    for value in values:
       if value > second_largest and value < largest:
           second_largest=value
    return second_largest

values = [3,4,5,6,9,2] #6
print(f"output:{find_second_largest(values)}")

values = [1] #None
print(f"output: {find_second_largest(values)}")

values = [3,4] #3
print(f"output: {find_second_largest(values)}")

values = [9,8,6,4,3,2] #8
print(f"output: {find_second_largest(values)}")
