# Complete the is_inside_bounds function which has the
# following parameters:
#   x: the x coordinate to check
#   y: the y coordinate to check
#   rect_x: The left of the rectangle
#   rect_y: The bottom of the rectangle
#   rect_width: The width of the rectangle
#   rect_height: The height of the rectangle
#
# The is_inside_bounds function returns true if all of
# the following are true
#   * x is greater than or equal to rect_x
#   * y is greater than or equal to rect_y
#   * x is less than or equal to rect_x + rect_width
#   * y is less than or equal to rect_y + rect_height

def is_inside_bounds(x, y, rect_x, rect_y, rect_width, rect_height):
    if x < rect_x or x > rect_x + rect_width:
        return False
    if y < rect_y or y > rect_y + rect_height:
        return False
    return True

print(is_inside_bounds(0,100,0,0,100,100))
print(is_inside_bounds(0,-10,0,0,100,100))
print(is_inside_bounds(101,0,0,0,100,100))
print(is_inside_bounds(-50,200,-50,0,100,200))
