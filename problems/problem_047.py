# Complete the check_password function that accepts a
# single parameter, the password to check.
#
# A password is valid if it meets all of these criteria
#   * It must have at least one lowercase letter (a-z)
#   * It must have at least one uppercase letter (A-Z)
#   * It must have at least one digit (0-9)
#   * It must have at least one special character $, !, or @
#   * It must have six or more characters in it
#   * It must have twelve or fewer characters in it
#
# The string object has some methods that you may want to use,
# like ".isalpha", ".isdigit", ".isupper", and ".islower"

def check_password(password):
    has_lowercase = False
    has_uppercase = False
    has_digit = False
    has_special = False
    has_length = len(password) > 5 and len(password) < 13
    for character in password:
        if character.isalpha():
            if character.islower():
                has_lowercase = True
            else:
                has_uppercase = True
        elif character.isdigit():
            has_digit = True
        elif character in "$!@":
            has_special = True
        else:
            print(f"Invalid character: {character}")
            return False
    return has_lowercase and has_uppercase and has_digit and has_special \
        and has_length

password= "aB$6gg"
print(check_password(password)) #true

password= "aB$6gggggggggggggggggggggggggggggggggggggggg"
print(check_password(password)) #false

password= "aB$6gg&"
print(check_password(password)) #false, print '&'

password= "aB$ggg"
print(check_password(password)) #false

password= "aBf6gg"
print(check_password(password)) #false

password= "aB$6g"
print(check_password(password)) #false
