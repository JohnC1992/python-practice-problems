class Book:
    def __init__(self, author, title) -> None:
        self.author = author
        self.title = title

    def get_author(self):
        return f"Author: {self.author}"

    def get_title(self):
        return f"Title: {self.title}"

book = Book("Sonia Shah","The Next Great Migration")
print(book.get_author())
print(book.get_title())
