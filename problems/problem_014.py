# Complete the can_make_pasta function to
# * Return true if the ingredients list contains
#   "flour", "eggs", and "oil"
# * Otherwise, return false
#
# The ingredients list will always contain three items.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def can_make_pasta(ingredients):
    if not "flour" in ingredients:
        return False
    if not "eggs" in ingredients:
        return False
    if not "oil" in ingredients:
        return False
    return True

ingredients1 = ["flour", "flour", "flour"]
ingredients2 = ["flour", "flour", "oil"]
ingredients3 = ["flour", "eggs", "flour"]
ingredients4 = ["eggs", "oil", "flour"]
ingredients5 = ["pasta", "sauce", "parmesan"]

print(can_make_pasta(ingredients1))
print(can_make_pasta(ingredients2))
print(can_make_pasta(ingredients3))
print(can_make_pasta(ingredients4))
print(can_make_pasta(ingredients5))
