# Write a function that meets these requirements.
#
# Name:       halve_the_list
# Parameters: a single list
# Returns:    two lists, each containing half of the original list
#             if the original list has an odd number of items, then
#             the extra item is in the first list
#
# Examples:
#    * input: [1, 2, 3, 4]
#      result: [1, 2], [3, 4]
#    * input: [1, 2, 3]
#      result: [1, 2], [3]

def halve_the_list(list):
    resultA = []
    resultB = []
    for count in range(len(list)):
        if count < len(list)/2:
            resultA.append(list[count])
        else:
            resultB.append(list[count])
    return resultA, resultB

listA = [1,2,3,4]
print(halve_the_list(listA))

listA = [1,2,3]
print(halve_the_list(listA))

listA = [1,2,3,4,7,6,8,6,5,4,5,6,7]
print(halve_the_list(listA))
