# Write a function that meets these requirements.
#
# Name:       count_word_frequencies
# Parameters: sentence, a string
# Returns:    a dictionary whose keys are the words in the
#             sentence and their values are the number of
#             times that word has appeared in the sentence
#
# The sentence will contain no punctuation.
#
# This is "case sensitive". That means the word "Table" and "table"
# are considered different words.
#
# Examples:
#    * sentence: "I came I saw I learned"
#      result:   {"I": 3, "came": 1, "saw": 1, "learned": 1}
#    * sentence: "Hello Hello Hello"
#      result:   {"Hello": 3}
def count_word_frequencies(sentence):
    words = sentence.split()
    result = {}
    for word in words:
        if word in result.keys():
            result[word] += 1
        else:
            result[word] = 1
    return result

sentence = "I am the end and the beginning"
print(count_word_frequencies(sentence).items())

sentence = "I am the evening and the morning star"
print(count_word_frequencies(sentence).items())

sentence = "My word is law"
print(count_word_frequencies(sentence).items())

sentence = "What I have is three halves half of which are half what you have"
print(count_word_frequencies(sentence).items())
