# Complete the calculate_grade function which accepts
# a list of numerical scores each between 0 and 100.
#
# Based on the average of the scores, the function
# returns
#   * An "A" for an average greater than or equal to 90
#   * A "B" for an average greater than or equal to 80
#     and less than 90
#   * A "C" for an average greater than or equal to 70
#     and less than 80
#   * A "D" for an average greater than or equal to 60
#     and less than 70
#   * An "F" for any other average
import problem_024

def calculate_grade(values):
    average = problem_024.calculate_average(values)
    if average >= 90:
        return "A"
    if average >= 80:
        return "B"
    if average >= 70:
        return "C"
    if average >= 60:
        return "D"
    return "F"

studentA_grades = [100,100,90,90]
studentB_grades = [100,100,0,0]
studentC_grades = [100,50,80,90]
studentD_grades = [70,70,70,80]

print(calculate_grade(studentA_grades)) #A
print(calculate_grade(studentB_grades)) #F
print(calculate_grade(studentC_grades)) #B
print(calculate_grade(studentD_grades)) #C
